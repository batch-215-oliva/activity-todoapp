<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
    if(Auth::check()){
        $tasks = Task::all();
        return view('task',compact('tasks'));
    } 
        $tasks = Task::all();
        return view('task',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

         if ($validator->fails())
        {
            return redirect()->route('tasks.index')->withErrors($validator);
        }


        $task = new Task(); //one row in your table
        $task->name = $request->input('name');
        $task->status = $request->input('status');
        // $task->user_id = auth()->user()->id;
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Task Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::check()){
            $task=Task::where('id',$id)->first();
            return view('edit-task',compact('task'));
        } else {

            return Redirect::route('login')->withInput()->with('errmessage', 'Please Login to access restricted area.');
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',

        ]);

        if ($validator->fails())
        {
            return redirect()->route('tasks.edit',['task'=>$id])->withErrors($validator);
        }



        $task=Task::where('id',$id)->first();
        $task->name=$request->get('name');
        $task->status=$request->get('status');
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Task Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::where('id',$id)->delete();
        return redirect()->route('tasks.index')->with('success', 'Task Deleted');
    }
}
