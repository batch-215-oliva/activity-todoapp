@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-lg-6">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{$error}}
                    </div>
                @endforeach
            @endif
        </div>
    </div>

@if(!Auth::guest())
 <div class="text-center mt-5">
    <h2>Add Task</h2>

    <form class="row g-3 justify-content-center " method="POST" action="{{route('tasks.store')}}">
        @csrf
        <div class="col-6">
            <input type="text" class="form-control" name="name" placeholder="Add Task">
            <input type="hidden" name="status" value="Pending">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary mb-3">Submit</button>   
        </div>          
    </form>    
</div>
    <div class="text-center">
        <h2>Tasks</h2>
        <div class="row justify-content-center">
            <div class="col-lg-6">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->name}}</td>                           
                            <td>
                                @if(($task->status) == 'Pending')
                                    <div class="badge bg-warning">Incomplete</div>                                   
                                @else
                                    <div class="badge bg-success">Done</div>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-primary" type="button" href="{{route('tasks.edit',['task'=>$task->id])}}" >Edit</a>
                                <a class="btn btn-danger" type="button" href="{{route('tasks.destory',['task'=>$task->id])}}">Delete</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@else
    <div class="text-center">
        <h2>Tasks</h2>
        <div class="row justify-content-center">
            <div class="col-lg-6">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->name}}</td>                           
                            <td>
                                @if(($task->status) == 'Pending')
                                    <div class="badge bg-warning">Incomplete</div>                                   
                                @else
                                    <div class="badge bg-success">Done</div>
                                @endif
                            </td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
</div>
</script>
@endsection